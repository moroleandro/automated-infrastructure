#!/bin/bash

URL=$TOKEN_WEBHOOK
JSON="{\"text\": \"*Deploy status:* \`"$1"\` $2 \n*Project name:* $CI_PROJECT_NAME\n*URL:* $CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID\"}"

curl -s -X POST -H 'Content-type: application/json' -d "$JSON" $URL 
