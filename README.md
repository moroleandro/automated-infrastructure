<h3 align="center">
    Terraform to deploy with Gitlab CI/CD
</h3>


## :bookmark: About

This repository was created to present a simple pipeline flow for delivering an EC2 instance to the AWS provider using terraform and terratest.
<a href="https://www.slideshare.net/LeandroMoro1/do-gitlab-ao-deploy-235640565" target="_blank">View slides</a>

## :page_facing_up: Technologies used

- Terraform v12
- TerraTest
- AWS CLI 

## :fire: How to use?

```sh
  #Config credencials
  aws configure

  #Export token webhook slack - https://hooks.slack
  export TOKEN_WEBHOOK=[same-token]
  
  #RUN Terratest
  cd test/ && go test -v ec2_test.go

  #RUN Terraform
  cd terraform/
  terraform init
  terraform plan
  terraform apply
``` 
---

<h4 align="center">
    Create by <a href="https://www.linkedin.com/in/moroleandro/" target="_blank">Leandro Moro</a>
</h4>
